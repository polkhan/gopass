# gopass

Manage gopass

## Dependencies

* [polkhan.go](https://gitlab.com/polkhan/go.git)
  _The go role is a dependency if you are installing gopass.
  The go configs must be included in the users `playbook.yml` since no
  default configs are passed to the dependent role_

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polkhan.gopass

## License

MIT
